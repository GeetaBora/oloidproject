import React, { Component } from 'react';
// import { Redirect } from 'react-router-dom'//  

export class Login extends Component{
    constructor(props){
        super(props);
        this.state={
            email:'',
            password:'',
            errors:'',
        };
        this.initialState = this.state;
    }

    validateForm=()=>{
        const{email,password}=this.state;
        let formIsValid= true;
        let errors ={};
        if(!email){
            formIsValid=false;
            errors["emailerror"]="Invalid email id";
        }
        if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email)) {
            formIsValid=false;
            errors["emailerror"]="Invalid email id";
        }
        var minMaxLength = /^[\s\S]{8,100}$/,
        upper = /[A-Z]/,
        lower = /[a-z]/,
        number = /[0-9]/
     

        if (!minMaxLength.test(password)) {
            formIsValid=false;
            errors["passerror"]="Invalid password";
        }
        if (!upper.test(password)) {
            formIsValid=false;
            errors["passerror"]="Invalid password";
        }
        if (!lower.test(password)) {
            formIsValid=false;
            errors["passerror"]="Invalid password";
        }
        if (!number.test(password)) {
            formIsValid=false;
            errors["passerror"]="Invalid password";
        }

        if (/(.)\1\1/.test(password)) {
            formIsValid=false;
            errors["passerror"]="Invalid password";
        }
        if(!password){
            formIsValid=false;
            errors["passerror"]="Invalid password";
        }
        this.setState({errors:errors});
        return formIsValid;
    }

    onSubmit=(e)=>{
        e.preventDefault();
        if(this.validateForm()){
            window.location.href ='/applications';
         //    history.push('/applications')
        }
        // this.setState(this.initialState)
    }

    handleChange=(e)=>{
        const{name,value}= e.target;
        this.setState({[name]:value});
    }
    
    render(){
        const{emailerror,passerror}= this.state.errors;
        return(
            <div>
                <div class="login-box">
                    <div class="login-txt">Login</div>
                    <form  onSubmit={this.onSubmit}>
                        <label htmlFor="email">Email</label><br></br>
                        <input
                            id="email"
                            name="email"
                            value={this.state.email}
                            onChange={this.handleChange}
                            placeholder="Enter your email"
                            className={emailerror?'input':''}
                        /><br></br>
                        {emailerror && <div style={{color:"red",}}>{emailerror}</div>}

                        <div className='pass-div'>
                            <label htmlFor="password">Password</label><br></br>
                            <input
                                name ="password"
                                id="password"
                                type="password"
                                placeholder="Enter your password"
                                value={this.state.password}
                                onChange={this.handleChange}
                                className={passerror ?'input':''}
                            /><br></br>
                        </div>
                        {passerror &&<div style={{color:"red",}}>{passerror}</div>}
                        
                        <div className='login-btn'>
                            <button className='submit'type="submit">Login</button>
                        </div>
                   
                    </form>
                </div>
            </div>
        )
    }
}
export default Login;
 