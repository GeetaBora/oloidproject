import React, { Component } from "react";
import './application.css';

import { Select} from 'antd';
const Option = Select.Option;
class Application extends Component{

    constructor (props){
        super(props);
        this.state={
            
          filter:{
            search:'',
            type:'ALL'
          }
        }
        

    }
    OnChangeHandler=(e)=>{
        const{filter}=this.state;
        const name=e.target.name;
        const value= e.target.value;
        filter[name]=value;
        this.setState({filter})

    }
    onsearch =(e)=>{
        const{dispatch}=this.props;
        const {filter}= this.state;
        if(e.key==='Enter'){
            let param={search:filter.search}
            if(filter.type!=='ALL'){
                param.type=filter.type;
            }
            // dispatch(filterApplication(param));
        }
    }
    onChangeAppType =(type)=>{
        const {filter} = this.state
        const {dispatch}= this.props;
        let param ={...filter}
        param.type='';
        filter.type=type;
        this.setState({filter});
        if(type!=='ALL'){
            param.type=type;
        }

    }
     
    render(){
        const{filter}=this.state;
        return(
          
                 <div >
                <div class="top-btn">
                    <h3 class="txt-application">Applications</h3>
                    <div class="create-btn">
                        <button  type="button" onClick={(e)=>{
                            e.preventDefault();
                            window.location.href='/createApplication';
                        }}
                          class="c-btn">Create Application
                          </button>
                    </div>
                </div>
               
                <div class="top-header">
                    <div class="search-box">
                     <div class="search-icon"></div>
                        <input 
                          id ="serch-btn"
                          placeholder="Search Application"
                          name="search"
                          value={filter.search}
                          onKeyDown={this.onsearch}
                          onChange={(e)=>{this.OnChangeHandler(e)}}
                           >
                        </input>
                    </div>
                </div>

                <div class="bottom-box">
                <div className='txt-customer'>Application Type</div>
                <Select defaultValue='All' className="select-input"
                    onChange={(e) => { this.onChangeAppType(e) }}
                    value={filter.type}
                    // getPopupContainer={trigger => trigger.parentNode}
                    >
                        <Option value="ALL">All</Option>
                        <Option value="Sticker">E-commerce</Option>
                        <Option value="Oloid Connect">Dtaa</Option>
                        <Option value="Oloid Verify">Development</Option>
                    </Select>
                     
                </div>
            </div>
           
           
           

        )
    }
}
export default Application;