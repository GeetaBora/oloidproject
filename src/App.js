import { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import Login from './login';
import Application from './Component/applications';
import ApplicationCreate from './Component/createApplication';
import Slideemailicon from './Component/slideemailicon'

class  App extends Component {
  render(){
    return (
      <div className="App">
         
         <Router>
            <Switch>
              <Redirect exact={true} from='/' to='/login' />
                <Route exact path='/login' name='Login Page' component={Login} /> 
                <Route path='/applications' name='Login Page' component={Application} /> 
                <Route path='/createApplication' name=' Create Application Page' component={ApplicationCreate}></Route>  
                <Route path='/slideemailicon' name='create slide' component={Slideemailicon}></Route>
            </Switch>
          </Router>
           
      </div>
      
    );
  }
  
}

export default App;
